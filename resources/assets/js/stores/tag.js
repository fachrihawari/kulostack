import services from './services';

// Make a store for the "tag"
export default {
    // You must define the name of the individual store
    name: "tag",
    // The state of the tag
    state: {
        current: [],
        all: []
    },
    set(val) {
        let me = this
        let keys = Object.keys(val)
        keys.forEach((key) => {
            me.state[key] = val[key]
        })
    },

    /*
    * Push Many to state 
    */
    pushMany(data){ 
        let me = this
        let keys = Object.keys(data)
        keys.forEach((key) => {
            data[key].forEach((item) => {
                me.state[key].push(item);
            })
        })        
    },

    removeMany(data) {
        let me = this
        let keys = Object.keys(data)
        keys.forEach((key) => {
            data[key].forEach((item) => {
                me.state[key].$remove(item);
            })
        })      
    }

}