<?php

return [

	'store.success' => 'Berhasil menambahkan data.',
	'update.success' => 'Berhasil memperbarui data.',
	'destroy.success' => 'Berhasil menghancurkan data.'

];