<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\Tag;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;

class DataController extends Controller
{

    private $token;

    public function __construct(Request $request)
    {

        /*
        * if user has login, set user as authenticated user
        */
        $header = 'authorization';
        $method = 'bearer';
        $header = $request->headers->get($header);

        if (! starts_with(strtolower($header), $method)) {
            return false;
        }

        $this->token = trim(str_ireplace($method, '', $header));

        if (!empty($this->token)) {
            JWTAuth::parseToken()->authenticate();
        }



    }

    public function getIndex(Request $request)
    {
        return response()->json([ 

            'tags' => [
                // 'all' => Tag::all(), 
                'current' => $this->token ?  auth()->user()->tags : []
            ]           

        ]);
    }
    public function getTags(Request $request)
    {
        if ($request->input('q')) {
            $tags = Tag::where('name', 'like', '%'.$request->input('q').'%')->skip($request->input('page'))->take(10)->get();
        } else {
            $tags = Tag::all();
        }
        $total_count = Tag::count();

        return response()->json([ 'tags' => $tags, 'total_count' => $total_count ]);
    }
}
