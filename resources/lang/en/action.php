<?php

return [

	'store.success' => 'Store data successfully.',
	'update.success' => 'Update data successfully.',
	'destroy.success' => 'Destroy data successfully.'

];