<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		
	    <link rel="stylesheet" href="{{ App::rev('css/app.css') }}">
	</head>

	<body>
		<div id="app"></div>

	    <script src="{{ App::rev('js/vendors.js') }}"></script>
	    <script src="{{ App::rev('js/main.js') }}"></script>
		@if(Session::has('message'))
		<script>			
			Materialize.toast("{{ Session::get('message') }}", 5000, 'custom_toast')
		</script>
		@endif
	</body>
</html>