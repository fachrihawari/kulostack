<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\TagRequest;
use App\Http\Controllers\Controller;
use App\Models\Tag;
use App\Models\TagUser;
use Illuminate\Http\Request;
use Lang;
use Exception;
use DB;

class TagController extends Controller
{


    /**
     * Get a all resource in storage.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->input('q')) {
            $tags = Tag::where('name', 'like', '%'.$request->input('q').'%')->skip($request->input('page'))->take(10)->get();
        } else {
            $tags = Tag::all();
        }
        $total_count = Tag::count();

        return response()->json([ 'tags' => $tags, 'total_count' => $total_count ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\API\TagRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        try {
            $tag = $request->all();
            $tag['slug'] = str_slug($request->name);
            $tag['user_id'] = auth()->id();
            $tag = Tag::create($tag);
            return response()->json([
                'message' => Lang::get('action.store.success'),
                'tag' => $tag
            ]);
        } catch (Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);            
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\API\TagRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TagRequest $request, $id)
    {
        $tag = Tag::findOrFail($id);
        try {
            $tag->name = $request->name;
            $tag->description = $request->description;
            $tag->user_id = auth()->id();
            $tag->slug = str_slug($request->name);
            $tag->save();
            return  response()->json([
                'message' => Lang::get('action.update.success'),
                'tag' => $tag
            ]);
        } catch (Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Tag::destroy($id);
            return  response()->json(['message' => Lang::get('action.destroy.success')]);
        } catch (Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);            
        }
    }

    /**
     * Get resource in storage by current user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTagByUser()
    {
        $tags = auth()->user()->tags;

        return response()->json([
            'tags' => $tags
        ]);   
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTagByUser(Request $request)
    {
        $tags = [];
        try {
            foreach ($request->tags as $key => $value) {

                $check = DB::table('tag_users')->where([
                    'tag_id' => $value,
                    'user_id' => auth()->id()
                ])->first();

                if ( is_null($check) ) {
                    $tag_user = new TagUser();
                    $tag_user->tag_id = $value;
                    $tag_user->user_id = auth()->id();
                    $tag_user->save();
                    $tags[] = $tag_user->tag;
                }

            }

            return response()->json([
                'tags' => $tags,
                'message' => Lang::get('action.store.success')
            ]);            
        } catch (Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyTagByUser($id)
    {
        try {
            DB::table('tag_users')->where([
                'user_id' => auth()->id(),
                'tag_id' => $id
            ])->delete();
            return  response()->json(['message' => Lang::get('action.destroy.success')]);
        } catch (Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);            
        }
    }

}
