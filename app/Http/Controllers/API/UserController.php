<?php

namespace App\Http\Controllers\API;

use Log;
use Lang;
use Storage;
use JWTAuth;
use Validator;
use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    use \Illuminate\Foundation\Auth\ResetsPasswords;

    public function index()
    {
        $users = User::all();
        $count = User::count();
        return response()->json(compact('users', 'count'));
    }

    /**
     * Log a user in.
     *
     * @param UserLoginRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        /*
        * Validate request
        */        
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        /*
        * Checking user
        */      
        try {
            if (!$token = JWTAuth::attempt($request->only('email', 'password'))) {
                return response()->json(['error' => Lang::get('auth.failed')], 401);
            }
        } catch (JWTException $e) {
            Log:error($e);
            return response()->json(['error' => Lang::get('jwt.could_not_create_token')], 500);
        }

        $user = User::where('email', $request->input('email'))->first();

        $tags = auth()->user()->tags;

        return response()->json(compact('token', 'user', 'tags'));
    }

    /**
     * Log the current user out.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        /*
        * remove current token of user
        */      
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
        } catch (JWTException $e) {
            Log:error($e);
            return response()->json(['error' => 'could_not_invalidate_token'], 500);
        }

        return response()->json();
    }

    /**
     * Create a new user.
     *
     * @param UserStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        /*
        * Validate request
        */        
        $this->validate($request, [
            'name' => 'required|max:50',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);


        /*
        * Store user to Database
        */           
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'activation_code' => md5(md5(md5(md5($request->input('email')))))
        ]);


        /*
        * Check if success storing user
        */
        if ($user) {

            /*
            * Triggering event UserRegistered
            */
            event(new \App\Events\UserRegistered($user));        
            
            /*
            * Checking user and auto login from current request
            */            
            try {
                if (!$token = JWTAuth::attempt($request->only('email', 'password'))) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {                
                Log:error($e);
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

        } else {
            return response()->json(['error' => 'could_not_create_user'], 422);
        }

        return response()->json(compact('token', 'user'));
    }


    /**
     * Create a new user.
     *
     * @param UserStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'password' => 'min:6|confirmed',
        ]);
        $user = User::find($id);
        try {
            $user->name = $request->name;
            $user->about = $request->about;
            $user->location = $request->location;
            $user->website = $request->website;
            $user->twitter = $request->twitter;
            $user->github = $request->github;
            $user->position = $request->position;
            $user->office = $request->office;
            $user->position = $request->position;

            if ($request->password) {                
                $user->password = bcrypt($request->password);
            }


            list($type, $photo) = explode(';', $request->binaryPhoto);
            list(, $photo)      = explode(',', $photo);        
            $base64ofimage = base64_decode($photo);

            $user->photo = empty($user->photo) ? str_slug($user->name).'.png': $user->photo;
            Storage::put(
                'images/users/'.$user->photo,
                $base64ofimage
            );      
            $user->save();      
            return  response()->json([
                'message' => Lang::get('action.update.success'),
                'user' => $user
            ]);      
        } catch (\Exception $e) {            
            return response()->json(['errors' => $e->getMessage()]);    
        }
    }

    public function activate($code='')
    {
        $user = User::where('activation_code', $code)->first();
        $user->activation_code = null;
        $user->save();
        return redirect('/')->withMessage(Lang::get('auth.account_actived'));
    }

    /**
     * Get the response for after the reset link has been successfully sent.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailSuccessResponse($response)
    {
        return response()->json(['message' => trans($response)]);
    }

    /**
     * Get the response for after the reset link could not be sent.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getSendResetLinkEmailFailureResponse($response)
    {
        return response()->json(['message' => trans($response)]);
    }

    /**
     * Get the response for after a successful password reset.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetSuccessResponse($response)
    {
        return response()->json(['message' => trans($response)]);
    }

    /**
     * Get the response for after a failing password reset.
     *
     * @param  string  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getResetFailureResponse($response)
    {
        return response()->json(['message' => trans($response)]);
    }

}
