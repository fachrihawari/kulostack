<?php

namespace App\Events;

use Mail;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class UserRegistered extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        Mail::send('emails.auth.confirmation', ['user' => $user], function ($m) use ($user) {
            $m->from(env('MAIL_USERNAME'), env('APP_NAME'));
            $m->to($user->email, $user->username)->subject('Confirmation!');
        });
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
