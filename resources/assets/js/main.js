// import components
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import VueSimpleStore from 'vue-simple-store';
import { configRouter } from './router/conf';

// Stores
import userStore from './stores/user.js'
import tagStore from './stores/tag.js'
import services from './stores/services.js';



window.moment = require('moment');

Vue.config.debug = true;
// Vue.config.devtools = false;

Vue.use(VueResource);
Vue.use(VueRouter)
Vue.use( VueSimpleStore, {
  stores: [ services, userStore, tagStore ], 
  debug: true 
});

// Init VueRouter
const router = new VueRouter({
    hashbang: false,
    history: true,
    saveScrollPosition: true,
    transitionOnLoad: true,
    linkActiveClass: 'active'
})


// Vue Resource config
Vue.http.options.root = '/api';
Vue.http.interceptors.push({
    request(r) {
        let kulostack = JSON.parse(window.localStorage.getItem('kulostack'));

        if ( kulostack != null && kulostack.token) {
            Vue.http.headers.common.Authorization = `Bearer ${kulostack.token}`;
        }

        return r;
    },

    response(r) {
        var kulostack = JSON.parse(window.localStorage.getItem('kulostack'));
        
        if (kulostack != null) {
            if (r.headers && r.headers.Authorization) {
                kulostack.token = r.headers.Authorization;
                localStorage.setItem('kulostack', JSON.stringify(kulostack));
            }

            if (r.data && r.data.token && r.data.token.length > 10) {
                kulostack.token = r.data.token;
                localStorage.setItem('kulostack', JSON.stringify(kulostack));
            }
        }

        return r;
    },
});


/*
* My Directive
*/
Vue.directive('select', {
    twoWay: true,
    priority: 1000,
    params: ['caption', 'uri', 'per_page'],
    bind () {
        let self = this

        /*
        * Get token and set to header ajax request
        */
        let kulostack = JSON.parse(window.localStorage.getItem('kulostack'));
        $.ajaxSetup({
            headers: {
                'Authorization': `Bearer ${kulostack.token}`
            }
        });                

        /*
        * Initialize Select2 plugin
        */
        $(this.el).select2({
            placeholder: "Pilih " + self.params.caption,
            language: "id-ID",                    
            minimumInputLength: 1,
            multiple: true,
            ajax: {
                url: "/api/" + self.params.uri,
                dataType: 'json',
                delay: 250,
                data (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.tags,
                        pagination: {
                            more: (params.page * self.params.per_page) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup (markup) {

                /*
                * Implements html template
                */                        
                return markup; 
            },  
            templateResult (data) {

                /*
                * Cek if load data done
                */
                if (data.loading) {
                    return data.text
                };

                /*
                * Set template autocomplete
                */
                let markup = 
                    "<h5>" + data.name + "</h5>"
                    +"<p>" + data.description + "</p>";

                return markup;
            }, 
            templateSelection (data) {
                return data.name;
            } 
        })
        .on('change', function () {
            self.set($(this).val())
        })
    },
    update (value) {
        $(this.el).val(value).trigger('change')
    },
    unbind () {
        $(this.el).off().select2('destroy')
    }            
})

// configure router
configRouter(router)

// boostrap the app
const App = Vue.extend(require('./components/App.vue'))
router.start(App, '#app')

