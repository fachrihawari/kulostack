import userStore from './user.js';
export default {
    name: "services",
    /**
      Set the Initial State
    */
    initial_state() {
        let fromLocalStorage = window.localStorage.getItem('kulostack')
            // If localStorage data is length
        if (fromLocalStorage !== null) {
            fromLocalStorage = JSON.parse(fromLocalStorage)
                // Destructuring First
            let {
                user: user,
                token: token
            } = fromLocalStorage
            // Set to user store
            userStore.set({
                current: user,
                token: token
            });
        }
    },
    /**
      get All state of the App
    */
    get_all() {
        let user = userStore.state.current
        let token = userStore.state.token
        let state = {
            user,
            token
        }
        return state
    },
    /**
      Backup All state to the Local Storage
    */
    backup_to_local_storage() {
        window.localStorage.setItem('kulostack', JSON.stringify(this.get_all()))
    },
    /*
     * Destroy local storage when user logout
     */
    destroy_local_storage() {
        window.localStorage.removeItem('kulostack');
    },
};