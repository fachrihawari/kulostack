<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'slug', 'user_id'
    ];

    /**
     * The users that belong to the user.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\user', 'tag_users');
    }
}
