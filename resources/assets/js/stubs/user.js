export default {
    "id": null,
    "name": null,
    "email": null,
    "address": null,
    "about": null,
    "website": null,
    "twitter": null,
    "github": null,
    "position": null,
    "place": null,
    "photo": null,
    "is_admin": null,
    "created_at": null,
    "updated_at": null
}