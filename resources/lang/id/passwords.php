<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Kata sandi harus 6 karakter atau lebih dan harus sama dengan konfirmasi password.',
    'reset' => 'Kata sandi anda berhasil di ubah!',
    'sent' => 'Kami telah mengirimkan link untuk merubah kata sandi!',
    'token' => 'Token tidak sah.',
    'user' => "Kami tidak bisa menemukan user dengan email address tersebut.",

];
