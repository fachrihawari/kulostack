<?php

use Illuminate\Database\Seeder;
class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('tags')->insert([
            [
                'name'        => 'Laravel',
                'slug'        => str_slug('Laravel'),
                'description'    => 'Laravel merupakan kerangka php yang elegan!',
                'user_id'=> 1,
                'created_at'=> new DateTime(),
                'updated_at'=> new DateTime()
            ],
            [
                'name'        => 'Codeigniter',
                'slug'        => str_slug('Codeigniter'),
                'description'    => 'Codeigniter merupakan kerangka php yang elegan!',
                'user_id'=> 1,
                'created_at'=> new DateTime(),
                'updated_at'=> new DateTime()
            ],
            [
                'name'        => 'PHP',
                'slug'        => str_slug('PHP'),
                'description'    => 'PHP merupakan bahasa pemrograman yang popular!',
                'user_id'=> 1,
                'created_at'=> new DateTime(),
                'updated_at'=> new DateTime()
            ],
            [
                'name'        => 'Ruby',
                'slug'        => str_slug('Ruby'),
                'description'    => 'Ruby juga merupakan bahasa popular!',
                'user_id'=> 1,
                'created_at'=> new DateTime(),
                'updated_at'=> new DateTime()
            ],
            [
                'name'        => 'Javascript',
                'slug'        => str_slug('Javascript'),
                'description'    => 'Javascript bahasa untuk membuat web interactive!',
                'user_id'=> 1,
                'created_at'=> new DateTime(),
                'updated_at'=> new DateTime()
            ],
        ]);
    }
}
