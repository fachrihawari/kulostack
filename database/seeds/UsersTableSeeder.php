<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
			'name'		=> 'Fachri Hawari',
            'location'  =>  'Tangerang',
            'website'   =>  'https://www.kulolab.com',
            'twitter'   =>  'https://twitter.com/fachri_netral',
            'position'  => 'Web Developer',
            'office'    => 'Monggo Tech',
            'about'     => '#### Perkenalkan 
```php 
public $name = "Fachri Hawari"; 
public $language = ["php", "javascript", "mysql"]; 
public $age = 17; 
```',
            'github'    =>  'https://github.com/fachrihawari',
            'photo'     => 'fachri-hawari.png',
			'email'		=> 'fachri.hawari@gmail.com',
			'is_admin'	=> 1,
			'password'	=> bcrypt('123456'),
            'created_at'=> new DateTime(),
            'updated_at'=> new DateTime()
    	]);
    }
}
