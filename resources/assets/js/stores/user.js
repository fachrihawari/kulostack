import services from './services';
import userStub from './../stubs/user';
// Make a store for the "user"
export default {
    // You must define the name of the individual store
    name: "user",
    // The state of the user
    state: {
        current: userStub,
        all: [],
        token: null
    },
    set(val) {
        let me = this
        let keys = Object.keys(val)
        keys.forEach((key) => {
            me.state[key] = val[key]
        })
        services.backup_to_local_storage()
    },
    resetCurrentUser() {
        this.state.current = userStub;
    }
}