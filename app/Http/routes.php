<?php


Route::get('activate/{code?}', 'API\UserController@activate');

Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {

    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');
    Route::post('forgot', 'UserController@postEmail');
    Route::post('reset', 'UserController@postReset');

    Route::controller('data', 'DataController');

    Route::group(['middleware' => 'jwt.auth'], function () {

	    Route::resource('tag', 'TagController', ['only' => ['index', 'store', 'update', 'destroy']]);
	    Route::get('tag/user', 'TagController@getTagByUser');
	    Route::post('tag/user', 'TagController@storeTagByUser');
	    Route::delete('tag/user/{id}', 'TagController@destroyTagByUser');
	    Route::get('user', 'UserController@index');
	    Route::resource('user', 'UserController', ['only' => ['index', 'update'] ]);

	});

});


Route::get('/image/{type}/{name?}', function ($type, $name=null)
{	
	if ($name == null) {
		$file = Storage::get('images/shared/no_image.png');	
	}
	else if (Storage::exists("images/users/{$name}")) {
		$file = Storage::get("images/users/{$name}");
	}
	else {
		$file = Storage::get('images/shared/no_image.png');
	}
	$result = Image::cache(function($image) use ($file) {
		if (Request::get('w')) {
		    $image->make($file)->resize(Request::get('w'), Request::get('h'), function ($constraint) {
	            $constraint->aspectRatio();
	        });
		} else {
			$image->make($file);
		}
	}, 10);		
	return response($result, 200)->header('Content-Type', 'image/png');				
});


Route::get('/{vue_capture?}', function () {
	return view('layout');
})->where('vue_capture', '[\/\w\.-@]*');
