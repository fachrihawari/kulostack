export default {
	/* 
	* @ Auth
	*/ 
	'/login': {
		component: require('./../components/auth/Login.vue')
	},
	'/register': {
		component: require('./../components/auth/Register.vue')
	},
	'/forgot': {
		component: require('./../components/auth/Forgot.vue')
	},
	'/reset/:token': {
		component: require('./../components/auth/Reset.vue')
	},
	'/logout': {
		component: require('./../components/auth/Logout.vue')
	},

	/*
	* @ Home 
	*/
	'/about': {
		component: require('./../components/main/About.vue')
	},
	'/contact': {
		component: require('./../components/main/ContactUs.vue')
	},
	'/': {
		component: require('./../components/main/Welcome.vue')
	},
	'/post': {
		subRoutes: {
			'/create': {
				component: require('./../components/post/create.vue');
			}
		}
	},

	/*
	* @ User
	*/
	'/user/:id': {
		auth: true,
		component: require('./../components/user/Index.vue'),
		subRoutes: {
			'/': {
				component: require('./../components/user/Summary.vue')
			},
			'/tag': {
				component: require('./../components/user/Tag.vue')
			},
			'/answer': {
				component: require('./../components/user/Answer.vue')
			},
			'/question': {
				component: require('./../components/user/Question.vue')
			},
		}
	},	

	/*
	* @ Admin
	*/
	'/admin': {
		adminOnly: true,
		auth: true,
		component: require('./../components/admin/Index.vue'),
		subRoutes: {
			'/': {
				component: require('./../components/admin/Dashboard.vue')
			},
			'/tag': {				
				component: require('./../components/admin/Tag.vue')
			},
			'/post': {
				component: require('./../components/admin/Post.vue')
			},
			'/user': {
				component: require('./../components/admin/User.vue')
			},
		}
	}	



}