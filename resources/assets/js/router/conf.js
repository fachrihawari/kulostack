// import map
import map from './map.js';

export function configRouter (router) {

  // Map
  router.map(map)

  // Handle not found routes
  router.redirect({ '*' : '/' })

  // Before Routing Events
  router.beforeEach(function(transition) {
    document.body.scrollTop = 0;
    let kulostack = JSON.parse(localStorage.getItem('kulostack'))
    if (transition.to.auth) {
      if (!kulostack.token || kulostack.token === null) {
        transition.redirect('/login')
      }
    }
    if (transition.to.adminOnly && kulostack.user && !kulostack.user.is_admin) {
      transition.redirect('/')
    }
    transition.next()
  })

}
