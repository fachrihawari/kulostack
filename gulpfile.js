var elixir = require('laravel-elixir');
var gutils = require('gulp-util');

require('laravel-elixir-vueify');

elixir.config.sourcemaps = false;

// Hot reload ENV checking
if(elixir.config.production == true){
    process.env.NODE_ENV = 'production';
}

// Hot reload checking
if(gutils.env._.indexOf('watch') > -1 && elixir.config.production != true){
    elixir.config.js.browserify.plugins.push({
        name: "browserify-hmr",
        options : {}
    });
}

elixir(function(mix) {

    // Define node dir
    var nodeDir = './node_modules/';

    // Compile js
    mix.browserify('main.js');
    
    // Compile css
    mix.sass('app.scss');

    // Mix css
    mix.scripts([
        nodeDir+'jquery/dist/jquery.min.js',
        nodeDir+'materialize-css/dist/js/materialize.min.js',
        nodeDir+'cropit/dist/jquery.cropit.js',
        nodeDir+'select2/dist/js/select2.min.js',
    ], 'public/js/vendors.js')

    mix.version(['js/vendors.js', 'js/main.js', 'css/app.css']);

    // Copy Font
    mix.copy(nodeDir+'materialize-css/fonts', 'public/build/fonts');

    if(gutils.env._.indexOf('watch') > -1 && elixir.config.production != true){
      mix.browserSync({
          proxy : 'kulolab.dev',
          public: [
              elixir.config.appPath + '/**/*.php',
              elixir.config.get('public.css.outputFolder') + '/**/*.css',
              elixir.config.get('public.versioning.buildFolder') + '/rev-manifest.json',
              // 'resources/assets/js/*.js'
              // 'resources/views/**/*.php',
          ],
      });
    }

});